<!DOCTYPE html>
<html>
	<head>
		<meta name="author" content="">
		<meta name="description" content="">
		<meta charset="utf-8">
		<title>Festival dos Festivais</title>
		<link rel="stylesheet" href="style.min.css">
	</head>
	<body>
		<header>
			<img src="images/logo_default.png">
			<nav id='topbar'>
				<ul>
					<li><span class='icon ticket' ></span>Reserva de Ingressos</li>
					<li><span class='icon reservedticket' ></span>Ingressos reservados</li>
					<li><span class='icon profile' ></span>Seu Perfil</li>
				</ul>
			</nav>
			<span class='exit' title='Sair'></span>
		</header>
		<div id="content">
			<h2> Título</h2>
			<fieldset>
				<form name="#" method="post" action="template_validation.php">
					<table>
						<tr>
							<td width="40%">Select:</td>
							<td>
								<select name="selselect">
									<option value="0">Escolha...</option>
									<option value="1">Opção 1</option>
									<option value="2">Opção 2</option>
									<option value="3">Opção 3</option>
								</select>
							</td>
						</tr>
						<tr>
							<td width="40%">Input de Texto:</td>
							<td><input type="text" name="txta" placeholder="aaaa-mm-dd" maxlength="10"></td>
						</tr>
						<tr>
							<td width="40%">Input de Texto:</td>
							<td><input type="text" name="txtb" placeholder="aaaa-mm-dd" maxlength="10"></td>
						</tr>

						<tr>
							<td colspan="2" align="center"><button type="reset">Limpar</button><button type="submit">Reservar</button></td>
						</tr>
					</table>
				</form>
			</fieldset>
		</div>
		<footer>
			&copy; Copyright 2015 Escola Sistêmica. Todos os direitos reservados.
		</footer>
	</body>
</html>
