<!DOCTYPE html>
<html>
	<head>
		<meta name="author" content="">
		<meta name="description" content="">
		<meta charset="utf-8">
		<title>Festival dos Festivais</title>
		<link rel="stylesheet" href="style.min.css">
	</head>
	<body>
		<header>
			<img src="images/logo_default.png">
			<nav id='topbar'>
				<ul>
					<li><span class='icon ticket' ></span>Reserva de Ingressos</li>
					<li><span class='icon reservedticket' ></span>Ingressos reservados</li>
					<li><span class='icon profile' ></span>Seu Perfil</li>
				</ul>
			</nav>
			<span class='exit' title='Sair'></span>
		</header>
		<div id="content">
			<h2> Aviso </h2>
			<?php

			$msg="Sua reserva foi efetuada!";
			$imagem="done_icon.png"; // alert_icon.png para erro.

			?>
			<div id='mensagem'>
				<h1><img src="images/<?php echo $imagem ?>" height='60px' width='60px'> <?php echo $msg; ?></h1>
				<a href="template.php"><button type="button">Retornar</button></a>
			</div>
		</div>
		<footer>
			&copy; Copyright 2015 Escola Sistêmica. Todos os direitos reservados.
		</footer>
	</body>
</html>
